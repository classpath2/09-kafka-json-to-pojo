package io.classpath.kafka.examples.consumer;

import io.classpath.kafka.examples.producer.AppConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;

public class StreamsConsumer {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        //1st step
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-app-demo");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.bootstrapServers);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.Integer().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        //Build the streams
        StreamsBuilder streamsBuilder = new StreamsBuilder();

        //transformation
        KStream<Integer, String> Ks01 = streamsBuilder.stream(AppConfig.topicName);

        //Ks01.peek((k,v) -> System.out.println(k));

        KStream<Integer, String> KS02 = Ks01.filter((key, value) -> ( key % 2 == 0) );

        //terminator operator
        KS02.foreach((key,value) -> System.out.println(" Key is "+ key + " value is : "+ value));

        Topology topology = streamsBuilder.build();

        KafkaStreams kafkaStreams = new KafkaStreams(topology, props);

        kafkaStreams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            kafkaStreams.close();;
        }));
    }
}