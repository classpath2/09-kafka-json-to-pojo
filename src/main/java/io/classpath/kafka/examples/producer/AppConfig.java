package io.classpath.kafka.examples.producer;

public class AppConfig {
    public final static String applicationID = "Producer-Demo";
    public final static String bootstrapServers = "68.183.95.195:9092,68.183.80.182:9092,68.183.88.114:9092";
    public final static String topicName = "even-numbers";
    public final static int numEvents = 1000;
}
