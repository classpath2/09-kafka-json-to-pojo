package io.classpath.kafka.examples.producer;

import io.classpath.kafka.examples.serde.UserSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Properties;

public class StreamsProducer {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) throws InterruptedException {
        logger.info("Creating Kafka Producer...");
        Properties props = new Properties();
        props.put(ProducerConfig.CLIENT_ID_CONFIG, AppConfig.applicationID);
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, AppConfig.bootstrapServers);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<Integer, String> producer = new KafkaProducer<>(props);

        logger.info("Start sending User data...");
        String user = null;
        for (int i = 1; i <= AppConfig.numEvents; i++) {
            Thread.sleep(2000);
            producer.send(new ProducerRecord<>(AppConfig.topicName, i, "Message "+i));
            logger.info("Successfully sent message to Broker:: Message "+i);
        }

        logger.info("Finished - Closing Kafka Producer.");
        producer.close();
    }
}